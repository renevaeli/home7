import java.util.*;

class Node {

   int frequency;
   byte byteCode;
   Node leftChild;
   Node rightChild;

   public Node () {}

   public Node (byte byteCode, int frequency) {
      this.byteCode = byteCode;
      this.frequency = frequency;
      this.leftChild = null;
      this.rightChild = null;
   }

   public byte getByteCode() {
      return byteCode;
   }

   public void setByteCode(byte byteCode) {
      this.byteCode = byteCode;
   }

   public int getFrequency() {
      return frequency;
   }

   public void setFrequency(int frequency) {
      this.frequency = frequency;
   }

   public Node getLeftChild() {
      return leftChild;
   }

   public void setLeftChild(Node leftChild) {
      this.leftChild = leftChild;
   }

   public Node getRightChild() {
      return rightChild;
   }

   public boolean isLeaf() {
      return this.rightChild == null && this.leftChild == null;
   }

   public void setRightChild(Node rightChild) {
      this.rightChild = rightChild;
   }

   @Override
   public String toString() {
      return "(B:" + this.byteCode +  " F:" + this.frequency + " L:" + leftChild + " R:" + rightChild + ")";
   }
}


class NodeComparator implements Comparator<Node> {
   @Override
   public int compare(Node a, Node b) {
      return   a.frequency - b.frequency;
   }
}


/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   Node root;
   private int bitLength = 0;
   private HashMap<Byte, boolean[]> encodingValues;
   //private HashMap<Byte, BitSet> encodingValues;
   private List<Node> leafNodes;
   //private List<Node> huffmanNodes;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
       root = new Node();
       leafNodes = new ArrayList<Node>();
       encodingValues = new HashMap<>();
       List<Node> huffmanNodes = new ArrayList<Node>();

      getLeaves(original);

      Collections.sort(leafNodes, new NodeComparator());

      printNodeList(leafNodes);

      huffmanNodes = leafNodes;

      while(huffmanNodes.size() > 1) {
         Node newNode = new Node();

         Node left = huffmanNodes.get(0);
         Node right = huffmanNodes.get(1);

         newNode.setFrequency(left.getFrequency() + right.getFrequency());
         newNode.setLeftChild(left);
         newNode.setRightChild(right);

         huffmanNodes.remove(left);
         huffmanNodes.remove(right);
         huffmanNodes.add(newNode);

         Collections.sort(huffmanNodes, new NodeComparator());
      }

      //printNodeList(huffmanNodes);
      root = huffmanNodes.get(0);

      if (root.isLeaf()) {
          encodingValues.put(root.getByteCode(), addBit(new boolean[0], false));
      } else {
          getBits(root, new boolean[0]);
      }

      // getBits(root, new BitSet(), 0);
      //System.out.println(encodingValues);
   }

   private void getLeaves(byte[] original) {
      for (byte b: original) {
         boolean listContainsNodeWithByte = false;

         for (Node node: leafNodes) {
            if (node.byteCode == b) {
               listContainsNodeWithByte = true;
               node.frequency++;
               //System.out.println("Found another " + b + ", frequency now " + node.frequency);
            }
         }

         if (!listContainsNodeWithByte) {
            //System.out.println("Creating new node for " + b);
            leafNodes.add(new Node(b, 1));
         }

      }
      //System.out.println(leafNodes.toString());
      //return leafNodes;
   }

   //inspireeritud buildCode meetodist https://algs4.cs.princeton.edu/55compression/Huffman.java.html
      public void getBits(Node node, boolean[] bits) {


      if (!node.isLeaf()) {
         getBits(node.getLeftChild(), addBit(bits, false));
         getBits(node.getRightChild(), addBit(bits, true));
      } else {
         //System.out.print("Byte: " + node.byteCode + " Bits: ");
         //printBits(bits);
         encodingValues.put(node.byteCode, bits);
      }
   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return bitLength;
   }

   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
       //System.out.println("Start endcoding");

      boolean[] encoding = new boolean[0];
      //StringBuffer encoding = new StringBuffer();

      for (int i = 0; i < origData.length; i++) {
          boolean[] code = encodingValues.get(origData[i]);

          for (int j = 0; j < code.length; j++) {
              boolean bit = code[j];
              encoding = addBit(encoding, bit);
          }
      }
      bitLength = encoding.length;
      //System.out.println("Bitlength after encoding: " +  bitLength);
      //printBits(encoding);

      //BitSet bitSet = toBitSet(encoding);
      //return bitSet.toByteArray();

       //printBytes(bitsToBytes(encoding));
       //System.out.println("Stop encoding");
       System.out.println("Bitstobytes result: ");
       printBytes(bitsToBytes(encoding));
       return bitsToBytes(encoding);
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {
       //System.out.println("Start decoding");
      List<Byte> decodedList = new ArrayList<Byte>();
      boolean[] encodedBits = bytesToBits(encodedData);

      //System.out.println("Encodedbits lenght: " + encodedBits.length);
       System.out.print("Print bits: ");
       printBits(encodedBits);
      //boolean[] decodedBits = bytesToBits(encodedData);

      Node temp = root;

      for (int i = 0; i < bitLength; i++) {
          if (root.isLeaf()) {
              decodedList.add(root.getByteCode());
          } else {
              if (encodedBits[i] == false) {
                  if (temp.getLeftChild().isLeaf()) {
                      decodedList.add(temp.getLeftChild().getByteCode());
                      temp = root;
                  } else {
                      temp = temp.getLeftChild();
                  }
              } else if (encodedBits[i] == true) {
                  if (temp.getRightChild().isLeaf()) {
                      decodedList.add(temp.getRightChild().getByteCode());
                      temp = root;
                  } else {
                      temp = temp.getRightChild();
                  }
              } else {
                  throw new RuntimeException("Something wrong with " + encodedData.toString());
              }
          }

      }
      //System.out.println("Stop decoding");
      return listToByteArray(decodedList);
   }

   //Abifuntsioonid
   static boolean[] addBit(boolean[] oldBits, boolean bit) {
       boolean[] newBits = new boolean[oldBits.length + 1];

       for (int i = 0; i < oldBits.length; i++) {
           newBits[i] = oldBits[i];
       }

       newBits[oldBits.length] = bit;
       return newBits;
   }

    public byte[] listToByteArray(List<Byte> list) {
      byte[] array = new byte[list.size()];
      for (int i = 0; i < list.size(); i++) {
         array[i] = list.get(i);
      }
      return array;
   }

   //https://stackoverflow.com/questions/26944282/java-boolean-to-byte-and-back
    public byte[] bitsToBytes(boolean[] bits) {
        //System.out.println("Start bitsToBytes");
        byte[] bytes;
        int index = 0;

        if (bits.length < 8) {
            bytes = new byte[1];
        } else if (bits.length >= 8 && (bits.length % 8) == 0) {
            bytes = new byte[bits.length / 8];
        } else {
            bytes = new byte[(bits.length / 8) + 1];
        }

        for (int i = 0; i < bytes.length; i++) {
            //System.out.println("i " + i);
           for (int j = 0; j < 8; j++) {
               //System.out.println("j " + j);
               if (index < bitLength) {
                   if (bits[index]) {
                       //set byte at position j in bytes[i] to 1
                       //NB! The bits in byte are in reverse order from bit array!
                       bytes[i] |= (1 << j);
                   } else {
                       bytes[i]  &= ~(1 << j);
                   }
                   index++;
               }
           }
            System.out.println(String.format("%8s", Integer.toBinaryString(bytes[i] & 0xFF)).replace(' ', '0'));
        }

        System.out.println("Length of bytes: " + bytes.length);
        printBytes(bytes);
        //System.out.println("End bitsToBytes");
        return bytes;
    }

    static boolean[] bytesToBits(byte[] bytes) {
        BitSet bits = BitSet.valueOf(bytes);
        boolean[] bools = new boolean[bytes.length * 8];
        for (int i = bits.nextSetBit(0); i != -1; i = bits.nextSetBit(i+1)) {
            bools[i] = true;
        }
        return bools;
    }

   static void printBytes(byte[] bytes) {
       System.out.print("Bytes: ");
      for (Byte b: bytes) {
         System.out.print(b.toString() + " ");
      }
       System.out.println();
   }

    static void printBits(boolean[] bits) {
        for (boolean b: bits) {
            System.out.print(b? 1 : 0);
        }
        System.out.println();
    }

   void printNodeList(List<Node> nodes) {
      for (Node node: nodes) {
         System.out.println(node.toString());
      }
   }


   /** Main method. */
   public static void main (String[] params) {
       runtest("AAAAAAAAAAAAABBBBBBCCCDDEEF");
      runtest("AAA");
      runtest("A");


   }

   static void runtest(String input) {
       String tekst = input;

       byte[] orig = tekst.getBytes();
       //printBytes(orig);

       Huffman huf = new Huffman (orig);

       byte[] kood = huf.encode (orig);
       //System.out.println("Encodedbytes length: " + kood.length);

       byte[] orig2 = huf.decode (kood);
       //printBytes(orig2);

       // must be equal: orig, orig2
       //System.out.println("Code bytelength: " + kood.length);
       //System.out.println("Orig bytelenght: " + orig.length);

       System.out.println (Arrays.equals (orig, orig2));

       int lngth = huf.bitLength();
       System.out.println ("Length of encoded data in bits: " + lngth);
   }
}

